# Herdt WebShop Navigation Wodrpress Plugin #

### Configuration ###

Plugin requires configuration about api url, that must be changes in file index.php
```
define( "HERDT_WS2_API_URL", 'http://api.webshop.herdt.local.com/category-tree?countryCode=');
```

### To display category tree paste below code into your template ###

```
#!php
do_action( 'herdt_ws2_nav_render_categories_action' );
```
