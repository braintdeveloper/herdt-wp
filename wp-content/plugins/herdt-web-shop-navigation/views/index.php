<?php
/**
 * @author Aleksey Stetsenko <astetseno@brightgrove.com>
 * Date: 10/12/2016
 * Time: 9:39 AM
 */
/** @var string $url */
/** @var string $urlWithCountry */
/** @var string $categories */

?>
<div id="herdtWebShopNav" style="display: none">

    <nav class="navbar-default navbar-fixed-top navbar">
        <div class="container">
            <ul class="navbar-nav navbar-left nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="false" aria-expanded="false">Catalog</a>
                    <?php echo $categories; ?>
                </li>
                <li><a href="<?php echo $url ?>">Shop</a></li>
                <li><a href="/de">DE</a></li>
                <li><a href="/at">AT</a></li>
                <li><a href="/ch">CH</a></li>
            </ul>
            <ul class="navbar-nav navbar-right nav">
                <li><a href="#">Mein HERDT</a></li>
                <li><a href="#">Mein Gutschein</a></li>
                <li><a href="#">Warenkorb</a></li>
                <li><a href="#">Newsletter</a></li>
                <li><a href="<?php echo $url; ?>/signup/index">Register</a></li>
                <li><a href="<?php echo $url; ?>/login">Login</a></li>
            </ul>
        </div>
    </nav>

</div>
