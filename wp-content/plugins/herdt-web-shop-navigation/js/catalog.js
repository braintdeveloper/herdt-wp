(function( $ ){

    // Init Dropdown menu
    $('#catalogTreeWrapper').on('click', '.dropdown-toggle', function (event){

        event.preventDefault();

        $(this).parent().toggleClass('open');
    });

    $.fn.catalog = function( options ) {

        var settings = $.extend( {
            'activeBlockClass' : 'active'
        }, options);

        var activeBlock = {};

        var bindLink = function (e) {

            e.preventDefault();

            e.stopPropagation();

            var targetBlockId = $(this).attr('rel-data');
            var targetBlock = $('#' + targetBlockId);

            // console.log(targetBlock);
            setActiveBlock(targetBlock);

        };

        var setActiveBlock = function(objBlock) {

            //console.log(activeBlock);

            activeBlock.removeClass('active');

            activeBlock = objBlock;

            activeBlock.addClass('active');

            // Todo: update "back" link

            activeBlock.find('a.badge, a.back').on('click', bindLink);

        };

        // ToDo: put the code into "init"
        activeBlock = this.children("ul:first");
        setActiveBlock(activeBlock);

        return this;

    };

})( jQuery );
