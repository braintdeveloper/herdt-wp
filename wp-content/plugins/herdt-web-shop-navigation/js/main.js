window.$ = jQuery;

jQuery(document).ready(function($){

	// Init Catalog
	$('#catalogTree').catalog({'location' : 'left'});

	// Init Catalog Tree max height
	$("#catalogTree").css({"max-height": $(window).height() * 0.7});

	// Init Custom Scrollbar
	$("#catalogTree").mCustomScrollbar({
		axis : "y",
		theme : "minimal-dark",
		scrollInertia: 30,
		autoHideScrollbar: true
	});


  $('#searchTypeSelect').on('click', 'ul li', function() {
    var el = $(this);
    $('#searchTypeInput').val(el.attr('data-value'));
    $('#searchTypeLabel').text(el.children('a').text());

    $('#searchInput').keyup();
  });
  $('#searchTypeSelect').on('show.bs.dropdown', function () {
    $("#searchSuggestions").closest(".dropdown").removeClass("open");
  });

});