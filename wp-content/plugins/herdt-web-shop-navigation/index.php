<?php
/*
Plugin Name: Herdt WebShop Navigation
#Plugin URI:  http://URI_Of_Page_Describing_Plugin_and_Updates
Description: This plugin fetches navigation categories menu from WS2 and displays it
Version: 1.0.0
#Author: Brightgrove
Author URI: http://www.brightgrove.com/
#License:     GPL2
#License URI: https://www.gnu.org/licenses/gpl-2.0.html
#Domain Path: /languages
Text Domain: HerdtWebShopNavigation
*/

/**
 * This plugin renders navigation tree
 *
 * To display category tree paste this code into your template
 * ```
 * do_action( 'herdt_ws2_nav_render_categories_action' );
 * ```
 *
 * Get data
 * - get from cache
 * - if false, fetch data from "web shop 2"
 * - set data to cache
 *
 * render menu using data
 *
 * Uninstall action must delete all cache of navigation
 */

/** Configuration */
$plugin_dir_url = plugin_dir_url(__FILE__);

define( "HERDT_WS2_URL", $plugin_dir_url);

// Once at 15 minutes
define( "HERDT_WS2_CACHE_EXPIRES", 60 * 15);

define( "HERDT_WS2_MAX_IN_3TH_LEVEL", 5);
define( "HERDT_WS2_MAX_IN_4TH_LEVEL", 8);
define( "HERDT_WS2_MAX_PRODUCTS_IN_5TH_LEVEL", 9);


/** Registration of main action */
add_action( 'herdt_ws2_nav_render_navigation_action', 'herdt_ws2_nav_render_navigation' );
add_action( 'herdt_ws2_nav_render_search_action', 'herdt_ws2_nav_render_search' );


/** Registration of install/uninstall trigger */
register_activation_hook( __FILE__, 'herdt_ws2_nav_install' );
register_deactivation_hook( __FILE__, 'herdt_ws2_nav_unistall' );

if ( !function_exists('herdt_ws2_nav_install' ) ) {
	function herdt_ws2_nav_install() {
	}
}

if ( !function_exists( 'herdt_ws2_nav_unistall' ) ) {
	function herdt_ws2_nav_unistall() {

	}
}


/** Fetching and rendering of navigation */
function herdt_ws2_nav_render_navigation() {
    $data = '<div id="herdtWebShopNav" style="display: none;">';

    // comment this for hiding header data
    $data .= herdt_ws2_nav_get_data();

    $data .= '</div>';

    herdt_ws2_nav_set_scripts();

	echo $data;
//	echo herdt_ws2_nav_get_menu($data);
}

/**
 * Return current country of wordpress site
 * @return string
 */
function herdt_ws2_nav_get_current_country() {
    // Default country
    $country = 'de';

    // Find country according to current site
    $details = get_blog_details();
    if (preg_match('/^\/([a-z]{2})\/$/', $details->path, $matches)) {
        $country = $matches[1];
    }

    return $country;
}

function herdt_ws2_nav_get_home_url($country = true) {
    $scheme = (get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-use-https', false)) ? 'https://' : 'http://';
    $baseUrl = $scheme . get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-url');

    if ($country === true) {
        $baseUrl .= '/' . herdt_ws2_nav_get_current_country();
    }

    return $baseUrl;
}

function herdt_ws2_nav_get_home_api_url() {
    $baseUrl = 'http://' . get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-api-url') . '/catalog?storeKey=' . herdt_ws2_nav_get_current_country();

    return $baseUrl;
}

function herdt_ws2_nav_get_data() {

    if ($token = $_GET['webshop-access-token']) {
        //setcookie('webshop-access-token', $token, 0);
    }


    $country = herdt_ws2_nav_get_current_country();

    $cacheKey = 'category-tree-' . $country;
    $cacheKeyExpires = 'category-tree-' . $country .'-expires-at';

//    $responseMessage = get_option($cacheKey);
    $responseMessage = false;
//    $expiresAt = get_option($cacheKeyExpires);

//    if (true || false === $responseMessage || ($expiresAt + HERDT_WS2_CACHE_EXPIRES < time())) {

//        $response = wp_remote_get( herdt_ws2_nav_get_home_api_url() , ['headers' => [
//            'Authorization' => 'Bearer ' . $token
//        ]]);

        $shopMenuUrl = herdt_ws2_nav_get_home_url() . '/wordpress/index';

//        $cookieParams = ['XDEBUG_SESSION' => 'XDEBUG_ECLIPSE'];
        $cookieParams = [];
        if (isset($_COOKIE['FRONTENDPHPSESSID'])) {
            $cookieParams['FRONTENDPHPSESSID'] = urlencode($_COOKIE['FRONTENDPHPSESSID']);
//            $cookieParams['XDEBUG_SESSION'] = urlencode($_COOKIE['XDEBUG_ECLIPSE']);
        }

        $headerParams = [];
        if (isset($_SERVER['REMOTE_ADDR'])) {
            $headerParams['X-Forwarded-For'] = $_SERVER['REMOTE_ADDR'];
//            $headerParams['X-Forwarded-For'] = '79.171.122.202'; // FOR TESTING "UA"
        }
//        if (isset($_COOKIE['_identity_frontend'])) {
//            $cookieParams['_identity_frontend'] = urlencode(stripslashes($_COOKIE['_identity_frontend']));
//        }

        $response = wp_remote_get($shopMenuUrl, [
            'cookies' => $cookieParams,
            'headers' => $headerParams,
        ]);

        if (!$response instanceof WP_Error
            && isset($response['response']['code'])
            && 200 == $response['response']['code']
        ) {
            $responseMessage = $response['body'];
            update_option( $cacheKey, $responseMessage );
            update_option( $cacheKeyExpires,  time() + HERDT_WS2_CACHE_EXPIRES);

            foreach ($response['cookies'] as $wpCookie) {
                if (in_array($wpCookie->name, ['FRONTENDPHPSESSID', '_identity_frontend'])) {
                    //setcookie($wpCookie->name, $wpCookie->value, $wpCookie->expires, $wpCookie->path, $wpCookie->domain);
                }
            }
        }

//    }

	return $responseMessage;
}

function herdt_ws2_nav_get_menu($data) {

    $html = '';

    if (empty($data)) {
        return '';
    }

    $json = json_decode($data, true);

    $categories = herdt_ws2_nav_prepare_menu_html($json);

    $html .= herdt_ws2_nav_render_php_file(__DIR__ . '/views/index.php', [
        'url' => herdt_ws2_nav_get_home_url(false),
        'categories' => $categories,
    ]);

    herdt_ws2_nav_set_scripts();

    return $html;
}

function herdt_ws2_nav_set_scripts() {


    // Set navigation main js
    wp_enqueue_script("herdt-navigation-main-js", HERDT_WS2_URL . "js/main.js", array("jquery"));
    wp_enqueue_script("bootstrap-js", HERDT_WS2_URL . "js/bootstrap.js", array("jquery"));
    wp_enqueue_script("catalog-js", HERDT_WS2_URL . "js/catalog.js", array("jquery"));

    // Set URL configuration
    $urlConfiguration = 'var HERDT_HOME_URL = "' . herdt_ws2_nav_get_home_url() . '";';
    wp_add_inline_script('herdt-navigation-main-js', $urlConfiguration);

    //set custom scroll bar
    wp_enqueue_style("customscrollbar-css", HERDT_WS2_URL . "customscrollbar/css/jquery.mCustomScrollbar.min.css", false);
    wp_enqueue_script("customscrollbar-js", HERDT_WS2_URL . "customscrollbar/js/jquery.mCustomScrollbar.concat.min.js", array());

    //set Typeahead
    wp_enqueue_script("typeahead-js", HERDT_WS2_URL . "js/typeahead.bundle.js", array("jquery"));
    wp_enqueue_style("typeadead-style", HERDT_WS2_URL . "css/typeadead.min.css", false);

    wp_enqueue_script("intercooler-js", HERDT_WS2_URL . "js/intercooler-1.0.0.js", array("jquery"));

    // Set navigation main styles
    wp_enqueue_style("style", HERDT_WS2_URL . "css/style.css", false, '1.1.1');
//    wp_enqueue_style("catalog-style", HERDT_WS2_URL . "css/catalog.css", false);

}

function herdt_ws2_nav_prepare_menu_html ($data){

    $html = '';

    $html .= '<div id="catalogTree" class="dropdown-menu">';

    if (isset($data[0]) && !empty($data[0])) {
        $html .= herdt_ws2_nav_render_branch($data[0]);
    }
    
    $html .= '</div>';

    return $html;
}


function herdt_ws2_nav_render_branch($branch) {

    $html = '';
    
    // Force to render first level of categories
    if ($branch['level'] == 1) {
        $html .= herdt_ws2_nav_render_first_level($branch);
        return $html;
    }

    $children = $branch['child'];

    if (empty($children) && isset($branch['product'])) {
        $html .= herdt_ws2_nav_render_products($branch);
        return $html;
    }

    $html .= '<ul id="branch' . $branch['id'] . '" class="list-group">';

    // Back button
    if ($branch['level'] > 2) {
        $html .= '<li class="list-group-item">';


        $backBtnParentId = ($branch['level'] == 3) ? 1 : $branch['parent_id'];
        $backBtnName = ($branch['level'] == 3) ? 'Back' : $branch['name'];
        $html .= '<a class="back" href="#" rel-data="branch' . $backBtnParentId.'">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>' . $backBtnName . '
                    </a>';
        $html .= '</li>';
    }

    foreach ($children as $child) {
        $html .= '<li class="list-group-item">';
        $html .= '<a class="badge" href="#" rel-data="branch' . $child['id'] . '">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                    </a>';
        $html .= '<a class="catalog-nav-link" href="' . $child['url'] . '">' . $child['name'] . '</a>';
        $html .= '</li>';
    }

    $html .= '</ul>';

    foreach ($children as $child) {
        $html .= herdt_ws2_nav_render_branch($child);
    }
    
    return $html;
}

/**
 * Renders first level of catalog
 * join first and second level of category
 * @param $branch
 */
function herdt_ws2_nav_render_first_level($branch) {
    $html = '';

    // list of category child that must be render after
    $listOfChild = [];

    $children = $branch['child'];

    if (empty($children)) {
        return;
    }

    $html .= '<ul id="branch' . $branch['id'] . '" class="list-group">';

    foreach ($children as $child) {
        $html .= '<li class="list-group-item small title-level-one">';
        $html .= '<strong>' . $child['name'] . '</strong>';
        $html .= '</li>';

        $childrenLevel3 = $child['child'];

        foreach ($childrenLevel3 as $childLevel3) {
            // push categories that must be shown after
            array_push($listOfChild, $childLevel3);

            $html .= '<li class="list-group-item">';

                $html .= '<a class="badge" href="#" rel-data="branch' . $childLevel3['id'] . '">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                </a>';

                $html .= '<a class="" href="' . $childLevel3['url'] . '">' . $childLevel3['name'] . '</a>';

            $html .= '</li>';
        }
    }

    $html .= '</ul>';

    foreach ($listOfChild as $row) {
        $html .= herdt_ws2_nav_render_branch($row);
    }

    return $html;
}

function herdt_ws2_nav_render_products($branch)
{

    $html = '';
    $products = $branch['product'];

    if (empty($products)) {
        return;
    }

    $html .= '<ul id="branch' . $branch['id'] . '" class="list-group">';

    // Back button
    $html .= '<li class="list-group-item">';
    $html .= '<a class="back" href="#" rel-data="branch' . $branch['parent_id'].'">
                <span class="fa fa-angle-left" aria-hidden="true"></span>' . $branch['name'] . '
             </a>';
    $html .= '</li>';

    foreach ($products as $product) {
        $html .= '<li class="list-group-item">';
        $html .= '<a class="catalog-nav-link" href="'.$product['url'].'">'.$product['title'].'</a>';
        $html .= '</li>';
    }

    $html .= '</ul>';

    return $html;
}

function herdt_ws2_nav_render_search()
{
    $html = '';

    $html .= '<div id="herdtSearchWidget" class="fusion-one-third wpcf7-form-control-wrap">';
    $html .= '<input type="text" class="form-control tt-input" name="q" placeholder="Search products" autocomplete="off" spellcheck="false" dir="auto" style="position: relative; vertical-align: top; background-color: transparent;" />';
    $html .= '</div>';

    echo $html;
}

function herdt_ws2_nav_render_php_file($_file_, $_params_ = [])
{
    ob_start();
    ob_implicit_flush(false);
    extract($_params_, EXTR_OVERWRITE);
    require($_file_);

    return ob_get_clean();
}
