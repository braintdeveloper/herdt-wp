<?php
/*
Controller name: Herdt menu
Controller description: Retrieve menus. Example of url /de/?json=herdt/get_top_menu.
```
assad
```
*/

/**
 * @author Aleksey Stetsenko <astetseno@brightgrove.com>
 * Date: 8/16/2016
 * Time: 4:53 PM
 */

class JSON_API_Herdt_Controller {

    // Gets all possible menus for API
    public function get_top_menu() {
        return $this->get_menu('Hauptmenü');
    }

    /**
     * Returns scope of footer information
     * @return array
     */
    public function get_footer() {
        $response = [];

        // Footer Box 1
        $response['footer_box_1'] = $this->get_footer_box_1();

        // Footer Box 2
        $response['footer_box_2'] = $this->get_footer_box_2();

        // Footer Box 3
        $response['footer_box_3'] = $this->get_footer_box_3();

        // Footer Copyright
        $response['footer_copyright'] = $this->get_footer_copyright();

        // Footer Icons
        $response['footer_icons'] = $this->get_footer_icons();

        return $response;
    }

    /**
     * Returns footer box 1
     * @return array
     */
    protected function get_footer_box_1() {
        ob_start();
        dynamic_sidebar('avada-footer-widget-' . 1);
        $response = ob_get_contents();
        ob_end_clean();

        return $response;
    }

    /**
     * Returns footer box 2
     * @return array
     */
    protected function get_footer_box_2() {
        ob_start();
        dynamic_sidebar('avada-footer-widget-' . 2);
        $response = ob_get_contents();
        ob_end_clean();

        return $response;
    }

    /**
     * Returns a text from footer box 3
     * @return string
     */
    protected function get_footer_box_3() {
//        $widgets = wp_get_sidebars_widgets();

        ob_start();
        dynamic_sidebar('avada-footer-widget-' . 3);
        $response = ob_get_contents();
        ob_end_clean();

        return $response;
    }

    /**
     * Returns footer copyright
     * @return array
     */
    protected function get_footer_copyright() {
        $response = Avada()->settings->get('footer_text');

        return $response;
    }

    /**
     * Return footer icons
     * @return array
     */
    protected function get_footer_icons() {
        $response = Avada()->settings->get( 'social_media_icons' );

        return $response;
    }

    /**
     * @param $name
     *
     * List of possible names:
     * Hauptmenü
     * Footer Widget 1
     * Footer Widget 2
     *
     * @return array
     */
    protected function get_menu($name) {
        global $json_api;

        if (null === $name) {
            $json_api->error("Name params is required.");
        }

        $array = wp_get_nav_menu_items($name);

        if (empty($array)) {
            $json_api->error("Nothing found.");
        }

        return $array;
    }
}
