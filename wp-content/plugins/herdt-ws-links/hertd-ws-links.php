<?php
/**
 * @package HERDT Webshop
 */
/*
Plugin Name: HERDT Webshop 2 Shortcodes
Description: Provide shortcodes to easy paste links to Webshop 2 categories and products in Wordpress posts and pages
Version: 1.0
Author: HERDT Development Team
Text Domain: herdt
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

// Hook for adding admin menus
add_action('admin_menu', 'herdt_add_pages');
//call register settings function
add_action( 'admin_init', 'register_herdt_settings' );

// action function for above hook
function herdt_add_pages() {

    // Add a new submenu under Options:
    add_options_page('HERDT WS2 Shortcodes Configuration', 'HERDT Shortcodes', 8, 'herdt-ws-links', 'herdt_ws_links_options_page');

}

function register_herdt_settings() {
    register_setting( 'herdt-ws-link-settings-group', 'webshop-url' );
    register_setting( 'herdt-ws-link-settings-group', 'webshop-api-url' );
    register_setting( 'herdt-ws-link-settings-group', 'webshop-use-https' );
    register_setting( 'herdt-ws-link-settings-group', 'webshop-default-link-text' );
    register_setting( 'herdt-ws-link-settings-group', 'webshop-default-link-open-in-new-tab' );
}

function herdt_ws_links_options_page() {
    ?>
    <div class="wrap">
        <h2>HERDT Webshop 2 Shortcodes Settings</h2>

        <form method="post" action="options.php">
            <?php settings_fields( 'herdt-ws-link-settings-group' ); ?>
            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Webshop base URL:</th>
                    <td><input type="text" name="webshop-url" value="<?php echo get_option('webshop-url'); ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Webshop API base URL:</th>
                    <td><input type="text" name="webshop-api-url" value="<?php echo get_option('webshop-api-url'); ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Use HTTPS:</th>
                    <td><input type="checkbox" name="webshop-use-https" value="1" <?php echo checked( 1, get_option( 'webshop-use-https' ), false ); ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Default link text:</th>
                    <td><input type="text" name="webshop-default-link-text" value="<?php echo get_option('webshop-default-link-text', 'Open in catalog'); ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row">Default link opens in new tab:</th>
                    <td><input type="checkbox" name="webshop-default-link-open-in-new-tab" value="1" <?php echo checked( 1, get_option( 'webshop-default-link-open-in-new-tab' ), false ); ?>" /></td>
                </tr>
            </table>

            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
            </p>

        </form>
    </div>
<?php } ?>

<?php

add_shortcode("ws-product", "shortcode_product_link_handler");
add_shortcode("ws-product-set", "shortcode_product_set_link_handler");
add_shortcode("ws-category", "shortcode_category_link_handler");

function shortcode_product_link_handler($atts, $content = null) {

    $scheme = (get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-use-https', false)) ? 'https://' : 'http://';
    $baseUrl = $scheme . get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-url');
    $openNewTabDefault = get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-default-link-open-in-new-tab') == 1 ? 'true' : 'false';

    $atts = shortcode_atts( array(0 => null, 1 => null, 'country' => 'de', 'button' => false, 'newtab' => $openNewTabDefault), $atts );

    $openNewTabTarget = $atts['newtab'] == "true" ? '_blank' : '_self';

    if (!$content) {
        $content = get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-default-link-text', 'Open in catalog');
    }

    $href = join('/', [$baseUrl, $atts['country'], 'product', $atts[0]]);
    if ($atts['button'] == 'true') {
        return '<a class="fusion-button button-flat button-round button-small button-darkgreen" href="' . $href . '" target="' . $openNewTabTarget . '">
            <span class="fusion-button-text">' . $content . '</span>
        </a>';
    } else {
        return '<a href="' . $href . '" target="' . $openNewTabTarget . '">' . $content . '</a>';
    }
}

function shortcode_product_set_link_handler($atts, $content = null) {

    $scheme = (get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-use-https', false)) ? 'https://' : 'http://';
    $baseUrl = $scheme . get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-url');
    $openNewTabDefault = get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-default-link-open-in-new-tab') == 1 ? 'true' : 'false';

    $atts = shortcode_atts( array(0 => null, 1 => null, 'country' => 'de', 'button' => false, 'newtab' => $openNewTabDefault), $atts );

    $openNewTabTarget = $atts['newtab'] == "true" ? '_blank' : '_self';

    if (!$content) {
        $content = get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-default-link-text', 'Open in catalog');
    }

    $href = join('/', [$baseUrl, $atts['country'], 'product-set', $atts[0]]);
    if ($atts['button'] == 'true') {
        return '<a class="fusion-button button-flat button-round button-small button-darkgreen" href="' . $href . '" target="' . $openNewTabTarget . '">
            <span class="fusion-button-text">' . $content . '</span>
        </a>';
    } else {
        return '<a href="' . $href . '" target="' . $openNewTabTarget . '">' . $content . '</a>';
    }
}

function shortcode_category_link_handler($atts, $content = null) {

    $scheme = (get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-use-https', false)) ? 'https://' : 'http://';
    $baseUrl = $scheme . get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-url');
    $openNewTabDefault = get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-default-link-open-in-new-tab') == 1 ? 'true' : 'false';

    $atts = shortcode_atts( array(0 => null, 1 => null, 'country' => 'de', 'button' => false, 'newtab' => $openNewTabDefault), $atts );

    $openNewTabTarget = $atts['newtab'] == "true" ? '_blank' : '_self';

    if (!$content) {
        $content = get_blog_option(BLOG_ID_CURRENT_SITE, 'webshop-default-link-text', 'Open in catalog');
    }


    $href = join('/', [$baseUrl, $atts['country'], 'category', $atts[0]]);
    if ($atts['button'] == 'true') {
        return '<a class="fusion-button button-flat button-round button-small button-darkgreen" href="' . $href . '" target="' . $openNewTabTarget . '">
            <span class="fusion-button-text">' . $content . '</span>
        </a>';
    } else {
        return '<a href="' . $href . '" target="' . $openNewTabTarget . '">' . $content . '</a>';
    }
}