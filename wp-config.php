<?php
/**
* The base configuration for WordPress
*
* The wp-config.php creation script uses this file during the
* installation. You don't have to use the web site, you can
* copy this file to "wp-config.php" and fill in the values.
*
* This file contains the following configurations:
*
* * MySQL settings
* * Secret keys
* * Database table prefix
* * ABSPATH
*
* @link https://codex.wordpress.org/Editing_wp-config.php
*
* @package WordPress
*/
// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */
//define('DB_NAME', 'wordpress_stg');
define('DB_NAME', 'brightgrove_herdt_wordpress_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
* Authentication Unique Keys and Salts.
*
* Change these to different unique phrases!
* You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
* You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
*
* @since 2.6.0
*/
define('AUTH_KEY',         '|!g]Uhj.+inF/Jq-C-JFP{b(0OwKTn(s&+`N|H@@eX?]Z.ynWk9tv!i#MLU$J2d2');
define('SECURE_AUTH_KEY',  'vj?So2Q]I!fhG@&Gfn{qhz>T|O;nI9?^X>x%<F_g]wX}P<*g>*I@B:FK8t~2FPh/');
define('LOGGED_IN_KEY',    'n71}yIYy.|]?a,Sv1?}Nr@d#z(X6H5L edTyi/9io?UAvFvCk2P4e&z-[=j]~*5{');
define('NONCE_KEY',        'lOtI(8^M 7CSWw*><w5]P|DJnVM*RB|(y:d_4WM:BRszs|QjO#_^1xI|bLI @lwi');
define('AUTH_SALT',        'xrQ-ijV_-&I709|oW:-96I$_AZA8GXc( 5fVLw6>x@sqWhj,|3JmE|!+bQ-a!5p{');
define('SECURE_AUTH_SALT', 'AJaSlThk6RpgS-R) 6GZk<~x,F;kY}8L+8r)kz*9339Ot#)b: |@J4Q*++Zux2|J');
define('LOGGED_IN_SALT',   'o)qnapw|.F[MY2U8<}l-Q8S1T0N?9_EYNyE Yc$IdmUo}t+*.6SPYX{]i2kixq,.');
define('NONCE_SALT',       ';Qozejrxv&`;;.!j8;VEA;Ns9X>v[6pJj,5dm?Z_wis7BHT-S|/Dr#6SClJY?gu+');
/**#@-*/
/**
* WordPress Database Table prefix.
*
* You can have multiple installations in one database if you give each
* a unique prefix. Only numbers, letters, and underscores please!
*/
$table_prefix  = 'stg392013_';
/**
* For developers: WordPress debugging mode.
*
* Change this to true to enable the display of notices during development.
* It is strongly recommended that plugin and theme developers use WP_DEBUG
* in their development environments.
*
* For information on other constants that can be used for debugging,
* visit the Codex.
*
* @link https://codex.wordpress.org/Debugging_in_WordPress
*/

/* Added admin-ssm, security feature */
define('DISALLOW_FILE_EDIT', false);

/* Added ssl for login and admin */
define('FORCE_SSL_LOGIN', false);
define('FORCE_SSL_ADMIN', false);

/* Increase memory limit */
define( 'WP_MEMORY_LIMIT', '256M' );

define('WP_DEBUG', false);

/* 20160226 admin-ssm */
/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'wordpress.herdt.local.com');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

define('WP_ALLOW_REPAIR', true);
/* That's all, stop editing! Happy blogging. */
/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
define('ABSPATH', dirname(__FILE__) . '/');
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
